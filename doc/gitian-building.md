Gitian building
================

This file was moved to [the Voyager Core documentation repository](https://github.com/voyager-core/docs/blob/master/gitian-building.md) at [https://github.com/voyager-core/docs](https://github.com/voyager-core/docs).
