Sample configuration files for:
```
SystemD: voyagerd.service
Upstart: voyagerd.conf
OpenRC:  voyagerd.openrc
         voyagerd.openrcconf
CentOS:  voyagerd.init
macOS:   org.voyager.voyagerd.plist
```
have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
