#!/usr/bin/env bash

export LC_ALL=C
TOPDIR=${TOPDIR:-$(git rev-parse --show-toplevel)}
BUILDDIR=${BUILDDIR:-$TOPDIR}

BINDIR=${BINDIR:-$BUILDDIR/src}
MANDIR=${MANDIR:-$TOPDIR/doc/man}

VOYAGERD=${VOYAGERD:-$BINDIR/voyagerd}
VOYAGERCLI=${VOYAGERCLI:-$BINDIR/voyager-cli}
VOYAGERTX=${VOYAGERTX:-$BINDIR/voyager-tx}
WALLET_TOOL=${WALLET_TOOL:-$BINDIR/voyager-wallet}
VOYAGERQT=${VOYAGERQT:-$BINDIR/qt/voyager-qt}

[ ! -x $VOYAGERD ] && echo "$VOYAGERD not found or not executable." && exit 1

# The autodetected version git tag can screw up manpage output a little bit
VYGVER=($($VOYAGERCLI --version | head -n1 | awk -F'[ -]' '{ print $6, $7 }'))

# Create a footer file with copyright content.
# This gets autodetected fine for voyagerd if --version-string is not set,
# but has different outcomes for voyager-qt and voyager-cli.
echo "[COPYRIGHT]" > footer.h2m
$VOYAGERD --version | sed -n '1!p' >> footer.h2m

for cmd in $VOYAGERD $VOYAGERCLI $VOYAGERTX $WALLET_TOOL $VOYAGERQT; do
  cmdname="${cmd##*/}"
  help2man -N --version-string=${VYGVER[0]} --include=footer.h2m -o ${MANDIR}/${cmdname}.1 ${cmd}
  sed -i "s/\\\-${VYGVER[1]}//g" ${MANDIR}/${cmdname}.1
done

rm -f footer.h2m
