// Copyright (c) 2011-2014 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef VOYAGER_QT_VOYAGERADDRESSVALIDATOR_H
#define VOYAGER_QT_VOYAGERADDRESSVALIDATOR_H

#include <QValidator>

/** Base58 entry widget validator, checks for valid characters and
 * removes some whitespace.
 */
class VoyagerAddressEntryValidator : public QValidator
{
    Q_OBJECT

public:
    explicit VoyagerAddressEntryValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

/** Voyager address widget validator, checks for a valid voyager address.
 */
class VoyagerAddressCheckValidator : public QValidator
{
    Q_OBJECT

public:
    explicit VoyagerAddressCheckValidator(QObject *parent);

    State validate(QString &input, int &pos) const;
};

#endif // VOYAGER_QT_VOYAGERADDRESSVALIDATOR_H
