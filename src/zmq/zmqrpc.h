// Copyright (c) 2018 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef VOYAGER_ZMQ_ZMQRPC_H
#define VOYAGER_ZMQ_ZMQRPC_H

class CRPCTable;

void RegisterZMQRPCCommands(CRPCTable& t);

#endif // VOYAGER_ZMQ_ZMRRPC_H
